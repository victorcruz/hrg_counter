/**
 * General functions.
 */

var is_mobile = false;
if (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i)
  || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i)
  || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i)) {
  is_mobile = true;
}

/**
 * Refresh and show connection state of socket.
 *
 * @param code Code of connection:
 * 0 -> connecting
 * 1 -> connect
 * 2 -> disconnect
 * 3 -> reconnecting
 */
function refreshConnectionState(code) {
  var $connection_state = $('#cf-connection-state');

  switch (code) {
    case 0:
      $connection_state
        .html('Conectándose al Servidor')
        .css('background-color', '#0b6e8b');
      break;
    case 1:
      $connection_state
        .html(' ') // Servidor Conectado
        .css({'background-color': '#447a44'});
      break;
    case 2:
      $connection_state
        .html(' ') // Error: Servidor Desconectado
        .css('background-color', '#be3935');
      break;
    case 3:
      $connection_state
        .html('Reconectando')
        .css('background-color', '#c7790a');
      break;
  }
};