/**
 * Model.
 */
'use strict';

angular.module("counter")

  .factory('Counter', ['Restangular', function (Restangular) {
    var route = 'counter';

    return {
      all    : Restangular.all(route),
      service: Restangular.service(route)
    }
  }])