'use strict';

// Declare app level module which depends on filters, and services.
angular.module('counter', [
  // Vendors modules.
  'ngResource',
  'ngRoute',
  'angular.filter',
  'ui.bootstrap',
  'ui.bootstrap.tpls',
  'ui.bootstrap.transition',
  'ngProgress',
  'restangular'
])

  // Restangular global configurations.
  .config(['RestangularProvider', function (RestangularProvider) {
    RestangularProvider

      .setBaseUrl('/api')

      .setRestangularFields({
        selfLink: 'url'
      })

      //.setDefaultHttpFields({cache: true})

      .setRequestInterceptor(function (elem, operation) {
        if (operation === "remove") {
          return null;
        }
        return elem;
      })

      .addResponseInterceptor(function (result, operation, what, url, response, deferred) {
        var extractedData;

        if (operation == 'getList') {
          extractedData = result.data;
        }
        else {
          extractedData = result;
        }

        return extractedData;
      });
  }])

  // Config of top progress bar.
  .config(['ngProgressProvider', function (ngProgressProvider) {
    // Default color is firebrick.
    ngProgressProvider.setColor('#333');
    // Default height is 2px.
    ngProgressProvider.setHeight('3px');
  }])

  // Routes of APP.
  .config(['$routeProvider', function ($routeProvider) {

    $routeProvider

      .otherwise({
        redirectTo: '/'
      });

    // TODO: VCA: Borrar esta línea si se llega a usar.
    //$locationProvider.hashPrefix('!');
  }])

  .run(['$rootScope', '$http', '$location', '$modal', '$log', '$timeout', 'ngProgress', function ($rootScope, $http, $location, $modal, $log, $timeout, ngProgress) {
    // Show or hide elements if error exist.
    $rootScope.error = false;

    // Show page load state, use corner indicator.
    $rootScope.showLoad = false;

    // Set project name.
    $rootScope.projectName = 'Contador';

    $rootScope.socket = io.connect(location.protocol + '//' + location.host);

    // Current date.
    $rootScope.currentDate = new Date(_.now());

    /**
     * Modal delete with message: Are you sure to delete this element?.
     * Reference: http://angular-ui.github.io/bootstrap/#/modal
     * Eg:
     * <button class="btn btn-default" ng-click="deleteModal(size, ctrl, tpl, element, index)">Delete</button>
     *
     * @param size      Size of modal ('sm' small or 'lg' large). eg: 'lg'.
     * @param css_class Extra class CSS.
     * @param ctrl      Any controller. eg: 'ApiIndicationsTemplatesController.delete'
     * @param tpl       Any Template by default is "delete_modal". eg: 'delete_modal'
     * @param element   Object to delete.
     * @param index     If element is into list get a index position in list.
     */
    $rootScope.deleteModal = function (size, css_class, ctrl, tpl, element, index) {
      var modalInstance = $modal.open({
        templateUrl: (tpl) ? tpl : 'delete_modal',
        controller : ctrl,
        size       : (size) ? size : 'lg',
        windowClass: (css_class) ? css_class : '',
        resolve    : {
          data: function () {
            return {
              element: element,
              index  : index
            };
          }
        }
      });

      modalInstance.result.then(function (result) {
        // TODO: VCA: Bloque que se ejecuta cuando se dé click en Ok.
      }, function () {
        // TODO: VCA: Bloque que se ejecuta cuando se dé click en Cancel.
      });
    };

    /**
     * Modal to show content from any controller with our template.
     * Reference: http://angular-ui.github.io/bootstrap/#/modal
     *
     * @param size          Size of modal ('sm' small or 'lg' large). eg: 'lg'.
     * @param css_class     Extra class CSS.
     * @param ctrl          Any controller. eg: 'ApiIndicationsTemplatesController.show'
     * @param tpl           Any Template. eg: 'indications_template_show'
     * @param element       Object to manipulate.
     * @param index     If element is into list get a index position in list.
     */
    $rootScope.openModal = function (size, css_class, ctrl, tpl, element, index) {
      var modalInstance = $modal.open({
        templateUrl: tpl,
        controller : ctrl,
        size       : (size) ? size : 'lg',
        windowClass: (css_class) ? css_class : '',
        resolve    : {
          data: function () {
            return {
              element: element,
              index  : index
            };
          }
        }
      });

      modalInstance.result.then(function (result) {
        // TODO: VCA: Bloque que se ejecuta cuando se dé click en Ok.
      }, function () {
        // TODO: VCA: Bloque que se ejecuta cuando se dé click en Cancel.
      });
    };

    $rootScope.$on('$routeChangeStart', function () {

      // Execute progress bar.
      ngProgress.start();
      $rootScope.showLoad = true;
    })

    $rootScope.$on('$routeChangeSuccess', function () {

      // Finish progress bar.
      ngProgress.stop();
      ngProgress.complete();
      $timeout(function () {
        $rootScope.showLoad = false;
      }, 1000);

      // Refresh menu page with current path.
      $rootScope.pathLocation = $location.path();
    });
  }])
