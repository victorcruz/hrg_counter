/**
 * Controller.
 */
'use strict';

angular.module("counter")

  .controller('counterCtrl', ['$scope', '$rootScope', '$interval', 'Counter', function ($scope, $rootScope, $interval, Counter) {
    // Dos formas de obtener la fecha:
    // moment(_.now());
    // new Date(_.now() * 1000);
    $interval(function () {
      $scope.datetimeShow = moment().locale('es').format("dddd DD MMMM YYYY h:mm A");
    }, 1000);

    $scope.batteryUrlImage = '/images/battery/00_0.svg';
    $scope.message = {};

    $scope.counter = 0;
    $rootScope.socket.emit('get all counters', function (data) {
      console.log('all counters: ', data)
    });

    /**
     * Start - Block Socket.
     */
    $rootScope.socket.on('connecting', function () {
      refreshConnectionState(0);
      $scope.message = {};
    });

    $rootScope.socket.on('connect', function () {
      refreshConnectionState(1);
      $scope.message = {};
    });

    $rootScope.socket.on('disconnect', function () {
      refreshConnectionState(2);

      $scope.message = {
        type: 'error',
        msg : 'Error: Servidor Desconectado'
      }
    });

    $rootScope.socket.on('update counter', function (value) {
      $scope.counter = value.length;
      $scope.message = {};

      $scope.lamp = $scope.counter * 252;
      $scope.moneybox = $scope.lamp * 9;

      if ($scope.counter >= 2000) {
        $scope.batteryUrlImage = '/images/battery/01_2000.svg';
      }

      if ($scope.counter >= 4000) {
        $scope.batteryUrlImage = '/images/battery/02_4000.svg';
      }

      if ($scope.counter >= 6000) {
        $scope.batteryUrlImage = '/images/battery/03_6000.svg';
      }

      if ($scope.counter >= 8000) {
        $scope.batteryUrlImage = '/images/battery/04_8000.svg';
      }

      if ($scope.counter >= 10000) {
        $scope.batteryUrlImage = '/images/battery/05_10000.svg';
      }
    });

    $rootScope.socket.on('set error camera', function (value) {
      $scope.message = {
        type: 'error',
        msg : value
      }
    });
    /**
     * End - Block Socket.
     */
  }])