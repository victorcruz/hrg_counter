/**
 * Routes.
 */
'use strict';

angular.module("counter")
  .config(['$routeProvider', function ($routeProvider) {

    /**
     * Routes
     */

    $routeProvider

      .when('/', {
        templateUrl: '/javascripts/app/partials/Counter/index.html',
        controller : 'counterCtrl'
      })
  }])