module.exports = function (grunt) {
  /*
   * Project configuration.
   */
  grunt.initConfig({
      pkg: grunt.file.readJSON('package.json'),

      mkdir: {
        build: {
          options: {
            create: ['build/<%= pkg.name %>'],
            mode  : '0775'
          }
        }
      },

      concat: {
        app: {
          src : [
            'public/javascripts/app/**/*.js'
          ],
          dest: 'public/javascripts/app.min.js'
        }
      },

      uglify: {
        app: {
          options: {
            sourceMap              : true,
            sourceMapIncludeSources: true
          },
          files  : {
            'public/javascripts/app.min.js': ['public/javascripts/app.min.js']
          }
        }
      },

      cssmin: {
        all: {
          files: [{
            expand: true,
            cwd   : 'public/stylesheets',
            src   : ['*.css', '!*.min.css'],
            dest  : 'public/stylesheets',
            ext   : '.min.css'
          }]
        }
      },

      watch: {
        scripts: {
          files  : [
            'public/javascripts/app/**/*.js',
            'public/stylesheets/**/*.css',
            'public/javascripts/app/partials/**'
          ],
          tasks  : [
            'app'
          ],
          options: {
            spawn: false
          }
        }
      },

      clean: {
        build: ['build/']
      }
    }
  );

  /*
   * Load plugins.
   */
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-compress');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-mkdir');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');

  /*
   * Custom tasks.
   */

  /**
   * Task: Tasks from Gulp to generate app.min.js to app from javascript files.
   */
  grunt.registerTask('app', [
    'concat:app',
    //'uglify:app',
    //'cssmin:all'
  ]);

  /*
   * Default tasks.
   */
  grunt.registerTask('default', []);
};