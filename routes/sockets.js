var io = require('socket.io');

// Models.
var Counter = require('../models/counter');

exports.socketServer = function (app, server) {
  var socket = io.listen(server);
  //socket.set('log level', 1);

  module.exports.export = socket;

  /**
   * Socket operations.
   */
  socket.sockets.on('connection', function (client) {
    console.log("New client is here!");

    //client.on('new user room', function (data) {
    //  room.new_user_room(io, client, data);
    //});

    client.on('get all counters', function (data) {
      Counter.all(function (docs) {
        client.emit('update counter', docs);
      });
    });
  });
};