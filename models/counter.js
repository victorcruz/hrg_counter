var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Counter = new Schema({
  data   : String,
  created: {
    type   : Date,
    default: Date.now
  }
});

/*
 * Method definitions.
 */

/**
 * List all counter.
 *
 * @param callback
 * @returns {Promise}
 */
Counter.statics.all = function (callback) {
  var this_model = this;

  return this_model.find({}, function (err, docs) {
    callback(docs);
  });
};

/**
 * Add new counter in the DB if not exist.
 *
 * @param counter
 * @param callback
 * @returns {*|find|type[]|Deferred|find|find}
 */
Counter.statics.add = function (counter, callback) {
  var this_model = this;

  var counterSave = new this_model({
    data: counter
  });

  return counterSave.save(function (error, doc) {
    if (!error) {
      callback(null, doc);
    }
    else {
      callback('Internal error while room save in DB.', null);
    }
  });
};

module.exports = mongoose.model('Counter', Counter);