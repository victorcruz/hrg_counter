var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var every = require('every-moment');
var wait = require('wait-one-moment');
var fs = require('fs');
var moment = require('moment');

var config = require('./config.json');
var routes = require('./routes/index');

// Models.
var Counter = require('./models/counter');

// Socket.
var socket = require('./routes/sockets');

var app = express();

// Set config as app variable.
app.set('config', config || {});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error  : err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error  : {}
  });
});

/**
 * Start - Block watch file.
 */
var prevDate = moment().locale('es').format("YYYY-MM-DD HH:mm:ss");

// Interval Time.
var timer = every(1, 'second', function () {
  //console.log(this.duration);

  fs.readFile('motion/count', 'utf8', function (err, data) {
    if (err) throw err;

    if (new Date(prevDate) < new Date(data)) {
      prevDate = data;

      Counter.add(prevDate, function (error, doc) {
        if (doc) {
          Counter.all(function (docs) {
            console.log('update counter');
            socket.export.sockets.emit('update counter', docs);
          });
        }
      });
    }
  });
});
timer.stop();

wait(3, 'seconds', function () {
  timer.start();
});

// Otra forma:
//var prevDataCount = 0;
//
//var timer = every(1, 'second', function () {
//  //console.log(this.duration);
//
//  fs.readFile('motion/count', 'utf8', function (err, data) {
//    if (err) throw err;
//
//    if (prevDataCount < data.length) {
//      prevDataCount = data.length;
//
//      for (var i = 0; i < data.length; i++) {
//        Counter.add(moment().locale('es').format("YYYY-MM-DD HH:mm:ss"), function (error, doc) {
//          if (doc) {
//            Counter.all(function (docs) {
//              console.log('update counter');
//              socket.export.sockets.emit('update counter', data);
//            });
//          }
//        });
//      }
//    }
//  });
//});
//timer.stop();
//
//wait(3, 'seconds', function () {
//  timer.start();
//});
/**
 * End - Block watch file.
 */

/**
 * Start - Block watch file with fs.watchFile.
 */
//fs.watchFile('motion/count', function (curr, prev) {
//  fs.readFile('motion/count', 'utf8', function (err, data) {
//    if (err) throw err;
//
//    Counter.add(data, function (error, doc) {
//      if (doc) {
//        Counter.all(function (docs) {
//          console.log('update counter');
//          socket.export.sockets.emit('update counter', docs);
//        });
//      }
//    });
//  });
//});

// Get errors.txt.
fs.watchFile('motion/errors.txt', function (curr, prev) {
  fs.readFile('motion/errors.txt', 'utf8', function (err, data) {
    if (err) throw err;

    console.log('Lanzar error cámara');
    socket.export.sockets.emit('set error camera', data);
  });
});
/**
 * End - Block watch file with fs.watchFile.
 */

module.exports = app;